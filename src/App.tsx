import React from 'react'
import Presentation from './containers/Presentation'

const App = () => {
    return <Presentation />
}

export default App
