import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles({
    tableCell: {
        width: '15%',
    },
})

export default useStyles
