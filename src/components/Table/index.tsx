import React from 'react'
import MuiTable from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { PresentationItem } from '../../entities/News'
import _ from 'lodash'
import useStyles from './Table.styles'

const ROWS = ['Url', 'Title', 'Timestamp', 'Story score', 'Author ID', 'Author karma']

interface TableProps {
    data: PresentationItem[]
}

const Table: React.FC<TableProps> = ({ data }) => {
    const classes = useStyles()
    return (
        <TableContainer component={Paper}>
            <MuiTable>
                <TableHead>
                    <TableRow>
                        {ROWS.map((row: string) => (
                            <TableCell key={row} align="center">
                                {row}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {_.orderBy(data, ['score'], ['asc']).map((item: PresentationItem) => (
                        <TableRow key={item.url}>
                            <TableCell className={classes.tableCell} align="center">
                                {(item.url && (
                                    <a href={item.url} target="_blank" rel="noreferrer">
                                        {item.url}
                                    </a>
                                )) ||
                                    'URL does not exist'}
                            </TableCell>
                            <TableCell className={classes.tableCell} align="center">
                                {item.title}
                            </TableCell>
                            <TableCell className={classes.tableCell} align="center">
                                {item.timestamp}
                            </TableCell>
                            <TableCell className={classes.tableCell} align="center">
                                {item.score}
                            </TableCell>
                            <TableCell className={classes.tableCell} align="center">
                                {item.authorId}
                            </TableCell>
                            <TableCell className={classes.tableCell} align="center">
                                {item.authorCarma}
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </MuiTable>
        </TableContainer>
    )
}

export default Table
