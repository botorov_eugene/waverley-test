import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loadingActionSelector } from '../../store/loader/loader.selectors'
import { GET_STORIES_REQUEST } from '../../store/stories/stories.types'
import { storiesDataSelector, storiesErrorSelector } from '../../store/stories/stories.selectors'
import Table from '../../components/Table'
import { getStoriesRequest } from '../../store/stories/stories.actions'
import CircularProgress from '@material-ui/core/CircularProgress'
import useStyles from './Presentation.styles'

const Presentation: React.FC = () => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const stories = useSelector(storiesDataSelector)
    const error = useSelector(storiesErrorSelector)
    const loading = useSelector(loadingActionSelector)([GET_STORIES_REQUEST])

    useEffect(() => {
        dispatch(getStoriesRequest())
    }, [dispatch])

    return (
        <div className={classes.root}>
            {loading ? (
                <CircularProgress />
            ) : error ? (
                'Something went wrong'
            ) : (
                <Table data={stories} />
            )}
        </div>
    )
}

export default Presentation
