export interface Story {
    by: string
    descendants: number
    id: number
    kids: number[]
    score: 111
    time: Date
    title: string
    type: string
    url: string
}

export interface User {
    about: string
    created: Date
    delay: number
    id: string
    karma: number
    submitted: number[]
}

export interface PresentationItem {
    title: string
    url: string
    timestamp: Date
    score: number
    authorId: string
    authorCarma: number
}
