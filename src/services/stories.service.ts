import axios from 'axios'

class StoriesApi {
    STORIES_API = 'https://hacker-news.firebaseio.com'

    getStoriesList = () => {
        return axios.get(`${this.STORIES_API}/v0/topstories.json`).then((res) => res.data)
    }

    getStory = (storyId: number) => {
        return axios.get(`${this.STORIES_API}/v0/item/${storyId}.json`).then((res) => res.data)
    }

    getAuthorInfo = (authorId: string) => {
        return axios.get(`${this.STORIES_API}/v0/user/${authorId}.json`).then((res) => res.data)
    }
}

export default new StoriesApi()
