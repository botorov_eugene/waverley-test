import { combineReducers } from 'redux'
import loader from './loader/loader.reducer'
import stories from './stories/stories.reducer'

export const rootReducer = combineReducers({
    loader,
    stories,
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
