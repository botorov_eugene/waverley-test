import { all, fork } from 'redux-saga/effects'
import news from './stories/stories.sagas'

export default function* root() {
    yield all([fork(news)])
}
