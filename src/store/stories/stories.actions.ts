import {
    CLEAR_STORIES_ERROR,
    GET_STORIES_FAILURE,
    GET_STORIES_REQUEST,
    GET_STORIES_SUCCESS,
    RESET_STORIES_TATE,
    StoriesActionsType,
} from './stories.types'
import { PresentationItem } from '../../entities/News'

export const getStoriesRequest = (): StoriesActionsType => ({
    type: GET_STORIES_REQUEST,
})

export const getStoriesSuccess = (payload: PresentationItem[]): StoriesActionsType => ({
    type: GET_STORIES_SUCCESS,
    payload,
})

export const GetStoriesFailure = (payload: string): StoriesActionsType => ({
    type: GET_STORIES_FAILURE,
    payload,
})

export const clearStoriesError = (): StoriesActionsType => ({
    type: CLEAR_STORIES_ERROR,
})

export const resetStoriesState = (): StoriesActionsType => ({
    type: RESET_STORIES_TATE,
})
