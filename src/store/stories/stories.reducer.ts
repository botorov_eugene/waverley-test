import {
    CLEAR_STORIES_ERROR,
    GET_STORIES_FAILURE,
    GET_STORIES_SUCCESS,
    RESET_STORIES_TATE,
    StoriesActionsType,
    StoriesState,
} from './stories.types'
import produce from 'immer'

const initialState: StoriesState = {
    data: [],
    error: null,
}

const StoriesReducer = (state = initialState, action: StoriesActionsType) =>
    produce(state, (draft) => {
        switch (action.type) {
            case GET_STORIES_SUCCESS:
                draft.data = action.payload
                break
            case GET_STORIES_FAILURE:
                draft.error = action.payload
                break
            case CLEAR_STORIES_ERROR:
                draft.error = initialState.error
                break
            case RESET_STORIES_TATE:
                draft = initialState
                break
        }
    })

export default StoriesReducer
