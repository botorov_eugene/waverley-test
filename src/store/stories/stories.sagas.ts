import { all, call, put, takeLatest } from 'redux-saga/effects'
import { GET_STORIES_REQUEST, GetStoriesRequest } from './stories.types'
import { startAction, stopAction } from '../loader/loader.actions'
import StoriesApi from '../../services/stories.service'
import { PresentationItem, Story } from '../../entities/News'
import { getStoriesSuccess } from './stories.actions'
import _ from 'lodash'

function* getStories({ type }: GetStoriesRequest) {
    yield put(startAction(type))
    try {
        const storiesList = yield call(StoriesApi.getStoriesList)
        const stories = yield all(
            _.sampleSize(storiesList, 10).map((item: number) => call(StoriesApi.getStory, item))
        )
        const authors = yield all(
            stories.map((item: Story) => call(StoriesApi.getAuthorInfo, item.by))
        )
        yield put(
            getStoriesSuccess(
                stories.map(
                    (story: Story, index: number): PresentationItem => ({
                        authorCarma: authors[index].karma,
                        authorId: authors[index].id,
                        score: story.score,
                        timestamp: story.time,
                        title: story.title,
                        url: story.url,
                    })
                )
            )
        )
    } catch (e) {
        console.log(e)
    } finally {
        yield put(stopAction(type))
    }
}

export default function* () {
    yield all([takeLatest(GET_STORIES_REQUEST, getStories)])
}
