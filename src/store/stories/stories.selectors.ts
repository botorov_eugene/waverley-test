import { RootState } from '../rootReducer'

export const storiesSelector = (state: RootState) => state.stories
export const storiesDataSelector = (state: RootState) => state.stories.data
export const storiesErrorSelector = (state: RootState) => state.stories.error
