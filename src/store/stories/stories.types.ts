import { PresentationItem } from '../../entities/News'

export const GET_STORIES_REQUEST = 'STORIES_GET_STORIES_REQUEST'
export const GET_STORIES_SUCCESS = 'STORIES_GET_STORIES_SUCCESS'
export const GET_STORIES_FAILURE = 'STORIES_GET_STORIES_FAILURE'
export const CLEAR_STORIES_ERROR = 'STORIES_CLEAR_ERROR'
export const RESET_STORIES_TATE = 'STORIES_RESET_STATE'

export interface StoriesState {
    data: PresentationItem[]
    error: null | string
}

export interface GetStoriesRequest {
    type: typeof GET_STORIES_REQUEST
}

export interface GetStoriesSuccess {
    type: typeof GET_STORIES_SUCCESS
    payload: PresentationItem[]
}

export interface GetStoriesFailure {
    type: typeof GET_STORIES_FAILURE
    payload: string
}

export interface ClearStoriesError {
    type: typeof CLEAR_STORIES_ERROR
}

export interface ResetStoriesState {
    type: typeof RESET_STORIES_TATE
}

export type StoriesActionsType =
    | GetStoriesFailure
    | GetStoriesRequest
    | GetStoriesSuccess
    | ClearStoriesError
    | ResetStoriesState
